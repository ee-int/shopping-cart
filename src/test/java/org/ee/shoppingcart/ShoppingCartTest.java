package org.ee.shoppingcart;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.ROUND_HALF_UP;
import static org.ee.shoppingcart.ShoppingCart.DEFAULT_PRECISION;
import static org.ee.shoppingcart.ShoppingCart.DEFAULT_ROUNDING_MODE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class ShoppingCartTest {

    public static final BigDecimal NO_OF_PRODUCTS = new BigDecimal(5);
    public static final String DOVE_SOAP_PRODUCT_NAME = "Dove Soap";
    public static final BigDecimal DOVE_PRODUCT_UNIT_PRICE = new BigDecimal(39.99);
    public static final String AXE_DEO_PRODUCT_NAME = "Axe Deo";
    public static final BigDecimal TAX = new BigDecimal(12.5);
    public static final BigDecimal AXE_DEO_PRODUCT_UNIT_PRICE = new BigDecimal(99.99);

    private ShoppingCart shoppingCart;
    private Product doveProduct;
    private Product axeDeoProduct;

    @Before
    public void setUp() {
        shoppingCart = new ShoppingCart();
        doveProduct = new Product(new Price(DOVE_PRODUCT_UNIT_PRICE), DOVE_SOAP_PRODUCT_NAME);
        axeDeoProduct = new Product(new Price(AXE_DEO_PRODUCT_UNIT_PRICE), AXE_DEO_PRODUCT_NAME);
    }

    @Test
    public void shouldGetTheTotalPriceOfFiveProductsOnTheShoppingCart() {
        shoppingCart.addProduct(doveProduct, NO_OF_PRODUCTS);

        int expectedNoOfProducts = 1;
        int expectedNoOfShoppingCartItems = 5;
        assertThat(shoppingCart.getProducts().size(), equalTo(expectedNoOfProducts));

        ShoppingCartItem shoppingCartItem = shoppingCart.getItemForProductWithName(DOVE_SOAP_PRODUCT_NAME);
        assertNotNull(shoppingCartItem);
        assertThat(shoppingCartItem.getProduct().getName(), is(DOVE_SOAP_PRODUCT_NAME));
        assertThat(shoppingCartItem.getProduct().getPrice().getValue(), is(DOVE_PRODUCT_UNIT_PRICE));


        assertThat(shoppingCartItem.getQuantity().intValue(), is(expectedNoOfShoppingCartItems));

        assertThat(shoppingCart.getTotalPrice(), equalTo(new BigDecimal(199.95).setScale(DEFAULT_PRECISION, ROUND_HALF_UP)));
    }

    @Test
    public void shouldGetTheTotalPriceOfEightProductsOnTheShoppingCart() {
        shoppingCart.addProduct(doveProduct, NO_OF_PRODUCTS);
        shoppingCart.addProduct(new Product(new Price(DOVE_PRODUCT_UNIT_PRICE), DOVE_SOAP_PRODUCT_NAME), new BigDecimal(3));

        int expectedNoOfProducts = 1;
        assertThat("shopping cart should have 1 products", shoppingCart.getProducts().size(), equalTo(expectedNoOfProducts));

        expectedNoOfProducts = 8;
        ShoppingCartItem shoppingCartItem = shoppingCart.getItemForProductWithName(DOVE_SOAP_PRODUCT_NAME);
        assertNotNull(shoppingCartItem);
        assertThat(shoppingCartItem.getProduct().getName(), is(DOVE_SOAP_PRODUCT_NAME));
        assertThat(shoppingCartItem.getProduct().getPrice().getValue(), is(DOVE_PRODUCT_UNIT_PRICE));

        assertThat(shoppingCartItem.getQuantity().intValue(), equalTo(expectedNoOfProducts));

        assertThat(shoppingCart.getTotalPrice(), equalTo(new BigDecimal(319.92).setScale(DEFAULT_PRECISION, DEFAULT_ROUNDING_MODE)));
    }

    @Test
    public void shouldGetTheTotalNoOfProductsAndTotalPriceWhenTwoDifferentProductsAreAddedWithVaryingQuantities() {
        BigDecimal noOfProductsToAdd = new BigDecimal(2);
        shoppingCart.addProduct(doveProduct, noOfProductsToAdd);
        shoppingCart.addProduct(axeDeoProduct, noOfProductsToAdd);

        assertThat(shoppingCart.getTotalNoOfProducts(), equalTo(4));
        assertThat(shoppingCart.getTotalPrice(), equalTo(new BigDecimal(279.96).setScale(DEFAULT_PRECISION, DEFAULT_ROUNDING_MODE)));
    }

    @Test
    public void shouldCalculateShoppingCartTotalPriceAndTaxWhenProductsWithTaxComponentAreAdded() {
        BigDecimal noOfProductsToAdd = new BigDecimal(2);
        doveProduct.setPrice(new Price(DOVE_PRODUCT_UNIT_PRICE, TAX));
        shoppingCart.addProduct(doveProduct, noOfProductsToAdd);

        axeDeoProduct.setPrice(new Price(AXE_DEO_PRODUCT_UNIT_PRICE, TAX));
        shoppingCart.addProduct(axeDeoProduct, noOfProductsToAdd);

        assertThat(shoppingCart.getTotalNoOfProducts(), equalTo(4));
        assertThat(shoppingCart.getTotalNoOfProducts(), equalTo(4));
        assertThat(shoppingCart.getTotalPrice(), equalTo(new BigDecimal(279.96).setScale(DEFAULT_PRECISION, DEFAULT_ROUNDING_MODE)));
        assertThat(shoppingCart.getTotalTax(), equalTo(new BigDecimal(35.0).setScale(DEFAULT_PRECISION, DEFAULT_ROUNDING_MODE)));
    }
}