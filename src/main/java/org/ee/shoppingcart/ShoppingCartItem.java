package org.ee.shoppingcart;

import java.math.BigDecimal;

import static org.ee.shoppingcart.ShoppingCart.DEFAULT_PRECISION;
import static org.ee.shoppingcart.ShoppingCart.DEFAULT_ROUNDING_MODE;

public class ShoppingCartItem {
    public static final int PERCENTAGE_DIVISOR = 100;
    private BigDecimal quantity;
    private Product product;

    public ShoppingCartItem() {
    }

    public ShoppingCartItem(Product product, BigDecimal quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getTotalPrice() {
        return this.getProduct().getPrice().getValue().multiply(getQuantity());
    }

    public BigDecimal getTotalTax() {
        BigDecimal tax =
                this.getProduct()
                        .getPrice()
                        .getTax()
                        .divide(new BigDecimal(PERCENTAGE_DIVISOR).setScale(DEFAULT_PRECISION, DEFAULT_ROUNDING_MODE));

        return this.getProduct()
                .getPrice()
                .getValue()
                .multiply(tax)
                .multiply(getQuantity())
                .setScale(DEFAULT_PRECISION, DEFAULT_ROUNDING_MODE);
    }
}
