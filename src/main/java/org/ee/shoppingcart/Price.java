package org.ee.shoppingcart;

import java.math.BigDecimal;

public class Price {
    public static final int DEFAULT_TAX_RATE = 0;
    private BigDecimal tax;
    private BigDecimal value;

    public Price(BigDecimal value) {
        this(value, new BigDecimal(DEFAULT_TAX_RATE));
    }

    public Price(BigDecimal value, BigDecimal tax) {
        this.value = value;
        this.tax = tax;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }
}
