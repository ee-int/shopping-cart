package org.ee.shoppingcart;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

public class ShoppingCart {
    public static final int DEFAULT_PRECISION = 2;
    public static final int DEFAULT_ROUNDING_MODE = BigDecimal.ROUND_HALF_UP;
    private List<ShoppingCartItem> shoppingCartItems;

    public ShoppingCart() {
        shoppingCartItems = new LinkedList<ShoppingCartItem>();
    }

    public void addProduct(Product product, BigDecimal quantity) {
        ofNullable(getItemForProductWithName(product.getName()))
                .map(item -> {
                    item.setQuantity(item.getQuantity().add(quantity));
                    return item;
                })
                .orElseGet(() -> {
                    ShoppingCartItem item = new ShoppingCartItem(product, quantity);
                    shoppingCartItems.add(item);
                    return item;
                });
    }

    public List<Product> getProducts() {
        return shoppingCartItems.stream().map(ShoppingCartItem::getProduct).collect(Collectors.toList());
    }

    public ShoppingCartItem getItemForProductWithName(String productName) {
        return this.shoppingCartItems.stream()
                .filter((item) -> item.getProduct().getName().equals(productName))
                .findFirst().orElse(null);
    }

    public BigDecimal getTotalPrice() {
        BigDecimal totalPrice =
                this.shoppingCartItems.stream()
                        .map(ShoppingCartItem::getTotalPrice)
                        .reduce(null, (current, next) -> {
                            if (current == null) return next;

                            return current.add(next);
                        });
        return totalPrice.setScale(DEFAULT_PRECISION, DEFAULT_ROUNDING_MODE);
    }

    public int getTotalNoOfProducts() {
        return this.shoppingCartItems.stream().mapToInt((item) -> item.getQuantity().intValue()).sum();
    }

    public BigDecimal getTotalTax() {
        return this.shoppingCartItems.stream()
                .map(ShoppingCartItem::getTotalTax)
                .reduce(null, (current, next) -> {
                    if (current == null) return next;

                    return current.add(next);
                })
                .setScale(DEFAULT_PRECISION, DEFAULT_ROUNDING_MODE);
    }
}
